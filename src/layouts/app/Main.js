import React, { useState } from 'react';
import axios from 'axios';
import useFetch from '../../helpers/useFetch';

// Set the token
const token = localStorage.getItem('token');

const Main = () => {
    const { data: users, isPending, error } = useFetch('http://localhost:8000/api/users', {
        headers: {
            'Authorization': `Bearer ${token}`
        }
    });

    const [formData, setFormData] = useState({
        name: '',
        email: '',
        password: '',
        password_confirmation: '',
        isAdmin: false,
    });
    const [editUserId, setEditUserId] = useState(null);

    const handleInputChange = (e) => {
        const { name, value } = e.target;
        setFormData({
            ...formData,
            [name]: value,
        });
    };

    const handleAddUser = async () => {
        try {
            await axios.post('http://localhost:8000/api/users', formData, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
        } catch (error) {
            console.error('Error adding user', error);
        }
    };

    const handleEditUser = (user) => {
        setFormData({
            name: user.name,
            email: user.email,
            password: '',
            password_confirmation: '',
            isAdmin: user.isAdmin,
        });
        setEditUserId(user.id);
    };

    const handleSaveChanges = async () => {
        try {
            if (editUserId) {
                await axios.put(`http://localhost:8000/api/users/${editUserId}`, formData, {
                    headers: {
                        'Authorization': `Bearer ${token}`
                    }
                });
            } else {
                await handleAddUser();
            }
        } catch (error) {
            console.error('Error saving changes', error);
        }
    };

    const handleDeleteUser = async (id) => {
        try {
            await axios.delete(`http://localhost:8000/api/users/${id}`, {
                headers: {
                    'Authorization': `Bearer ${token}`
                }
            });
        } catch (error) {
            console.error('Error deleting user', error);
        }
    };

    const resetForm = () => {
        setFormData({
            name: '',
            email: '',
            phone: '',
            password: '',
            password_confirmation: '',
            isAdmin: false,
        });
        setEditUserId(null);
    };

    return (
        <main id="main" className="main">
            <section className="section dashboard">
                <div className="row">
                    <div className="container bg-white p-3">
                        <h4>Users</h4>
                        <button type="button" className="btn float-end btn-sm btn-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                            Add User
                        </button>

                        <div className="modal fade" id="exampleModal" tabIndex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                            <div className="modal-dialog">
                                <div className="modal-content">
                                    <div className="modal-header">
                                        <h1 className="modal-title fs-5" id="exampleModalLabel">{editUserId ? 'Edit User' : 'Add User'}</h1>
                                        <button type="button" className="btn-close" data-bs-dismiss="modal" aria-label="Close" onClick={resetForm}></button>
                                    </div>
                                    <div className="modal-body">
                                        <form>
                                            <div className="mb-3">
                                                <label htmlFor="name" className="form-label">Name</label>
                                                <input type="text" className="form-control" id="name" name="name" value={formData.name} onChange={handleInputChange} />
                                            </div>
                                            <div className="mb-3">
                                                <label htmlFor="email" className="form-label">Email</label>
                                                <input type="email" className="form-control" id="email" name="email" value={formData.email} onChange={handleInputChange} />
                                            </div>
                                            <div className="mb-3">
                                                <label htmlFor="phone" className="form-label">Phone</label>
                                                <input type="phone" className="form-control" id="phone" name="phone" value={formData.phone} onChange={handleInputChange} />
                                            </div>
                                            <div className="mb-3">
                                                <label htmlFor="password" className="form-label">Password</label>
                                                <input type="password" className="form-control" id="password" name="password" value={formData.password} onChange={handleInputChange} />
                                            </div>
                                            <div className="mb-3">
                                                <label htmlFor="password_confirmation" className="form-label">Confirm Password</label>
                                                <input type="password" className="form-control" id="password_confirmation" name="password_confirmation" value={formData.password_confirmation} onChange={handleInputChange} />
                                            </div>
                                        </form>
                                    </div>
                                    <div className="modal-footer">
                                        <button type="button" className="btn btn-secondary" data-bs-dismiss="modal" onClick={resetForm}>Close</button>
                                        <button type="button" className="btn btn-primary" onClick={handleSaveChanges}>Save changes</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {error && <div>{error}</div>}
                        {isPending && <div>Loading...</div>}
                        {users && (
                            <table className="table">
                                <thead className="table-light">
                                <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Name</th>
                                    <th scope="col">Email</th>
                                    <th scope="col">Actions</th>
                                </tr>
                                </thead>
                                <tbody>
                                {users.map((user, index) => (
                                    <tr key={user.id}>
                                        <th scope="row">{index + 1}</th>
                                        <td>{user.name}</td>
                                        <td>{user.email}</td>
                                        <td>
                                            <button className="btn btn-sm btn-warning me-2" onClick={() => handleEditUser(user)} data-bs-toggle="modal" data-bs-target="#exampleModal">Edit</button>
                                            <button className="btn btn-sm btn-danger" onClick={() => handleDeleteUser(user.id)}>Delete</button>
                                        </td>
                                    </tr>
                                ))}
                                </tbody>
                            </table>
                        )}
                    </div>
                </div>
            </section>
        </main>
    );
};

export default Main;
