import React, { useState } from 'react';
import axios from 'axios';

const Login = ({ onLogin }) => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errors, setErrors] = useState({});

    const handleSubmit = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post('http://localhost:8000/api/auth/login', { email, password });
            const token = response.data.token;
            localStorage.setItem('token', token);
            onLogin();
        } catch (error) {
            if (error.response && error.response.data && error.response.data.errors) {
                setErrors(error.response.data.errors.reduce((acc, curr) => {
                    acc[curr.path] = curr.msg;
                    return acc;
                }, {}));
            } else {
                setErrors({});
            }
        }
    };

    return (
        <div className="login-container">
            <div className="container">
                <div className="col-6 mx-auto mt-5">
                    <div className="card mb-3">
                        <div className="card-body">
                            <div className="pt-4 pb-2">
                                <h5 className="card-title text-center pb-0 fs-4">Login to Your Account</h5>
                                <p className="text-center small">Enter your email & password to login</p>
                            </div>
                            {errors && Object.keys(errors).length > 0 && (
                                <div className="alert alert-danger">
                                    {Object.keys(errors).map((key, index) => (
                                        <p key={index}>{errors[key]}</p>
                                    ))}
                                </div>
                            )}
                            <form className="row g-3 needs-validation" noValidate onSubmit={handleSubmit}>
                                <div className="col-12">
                                    <label htmlFor="email" className="form-label">Email</label>
                                    <div className="input-group has-validation">
                                        <input
                                            type="email"
                                            name="email"
                                            className={`form-control ${errors.email && 'is-invalid'}`}
                                            id="email"
                                            value={email}
                                            onChange={(e) => setEmail(e.target.value)}
                                            required
                                        />
                                        {errors.email && <div className="invalid-feedback">{errors.email}</div>}
                                    </div>
                                </div>
                                <div className="col-12">
                                    <label htmlFor="yourPassword" className="form-label">Password</label>
                                    <input
                                        type="password"
                                        name="password"
                                        className={`form-control ${errors.password && 'is-invalid'}`}
                                        id="yourPassword"
                                        value={password}
                                        onChange={(e) => setPassword(e.target.value)}
                                        required
                                    />
                                    {errors.password && <div className="invalid-feedback">{errors.password}</div>}
                                </div>
                                <div className="col-12">
                                    <button className="btn btn-primary w-100" type="submit">Login</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Login;
