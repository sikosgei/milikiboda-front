import React, { useState, useEffect } from 'react';
import axios from 'axios';
import Header from './layouts/app/Header';
import Sidebar from './layouts/app/Sidebar';
import Main from './layouts/app/Main';
import Footer from './layouts/app/Footer';
import Login from './layouts/auth/Login';

function App() {
    const [isLoggedIn, setIsLoggedIn] = useState(false);

    useEffect(() => {
        const token = localStorage.getItem('token');
        if (token) {
            setIsLoggedIn(true);
        } else {
            setTimeout(() => {
                setIsLoggedIn(false);
            } , 5000);
        }
    }, [isLoggedIn]);

    const handleLogin = () => {
        setIsLoggedIn(true);
    };

    return (
        <div className="App">
            {isLoggedIn ? (
                <>
                    <Header />
                    <Sidebar />
                    <Main />
                    <Footer />
                </>
            ) : (
                <Login onLogin={handleLogin} />
            )}
        </div>
    );
}

export default App;
